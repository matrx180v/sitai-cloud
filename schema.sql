CREATE TABLE classe(
    id VARCHAR(3),
    section VARCHAR(255),
    cycle VARCHAR(255),
    level VARCHAR(255),
    serie VARCHAR(255),
    numero VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE payment(
    id VARCHAR(255),
    name VARCHAR(255),
    sexe VARCHAR(50),
    contact VARCHAR(255),
    matricule VARCHAR(20),
    idclasse VARCHAR(3),
    date timestamp default now(),
    amount FLOAT default 0,
    academicYear VARCHAR(20),
    PRIMARY KEY (id),
    Constraint fk_payment_classe Foreign key (idclasse) references classe(id) ON DELETE CASCADE ON UPDATE CASCADE 
)