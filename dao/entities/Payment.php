<?php
include_once "Classe.php";

class Payment{
    public $id;
    public $name;
    public $sexe;
    public $contact;
    public $matricule;
    public $classe;
    public $date;
    public $amount;
    public $academicYear;


    function toJson(){
        return array(
            'id' => $this->id,
            'name' => $this->name,
            'sexe' => $this->sexe,
            'contact' => $this->contact,
            'matricule' => $this->matricule,
            'classe' => isset($this->classe)?$this->classe->toJson():null,
            'date' => $this->date,
            'amount' => $this->amount,
            'academicYear' => $this->academicYear
        );
    }

    function setFromJson($json){
        if(isset($json['classe'])){
            $_classe = new Classe();
            $this->classe = $_classe->setFromJson($json['classe']);
        }
        $this->id = $json['id'];
        $this->name = $json['name'];
        $this->sexe = $json['sexe'];
        $this->contact = $json['contact'];
        $this->matricule = $json['matricule'];
        $this->date = $json['date'];
        $this->amount = $json['amount'];
        $this->academicYear = $json['academicYear'];
        return $this;
    }

    function setFromResultSet($row){
        if(isset($row['idclasse'])){
            $_classe = new Classe();
            $_classe->id = $row['idclasse'];
            $this->classe = $_classe;
        }
        $this->id = $row['id'];
        $this->name = $row['name'];
        $this->sexe = $row['sexe'];
        $this->contact = $row['contact'];
        $this->matricule = $row['matricule'];
        $this->date = $row['date'];
        $this->amount = $row['amount'];
        $this->academicYear = $row['academicYear'];
        return $this;
    }

    function removePayment($id){
        return "DELETE FROM payment WHERE id='".preg_replace("/'/","''",$id)."'";
    }

    function removeProfil($matricule){
        return "DELETE FROM payment WHERE matricule='".preg_replace("/'/","''",$matricule)."'";
    }
    
    function addPayment(){
        return "INSERT INTO payment(id,name, matricule, sexe, contact, idclasse, date, amount, academicYear) VALUES('".$this->id."', '".preg_replace("/'/","''",$this->name)."', '".preg_replace("/'/","''",$this->matricule)."', '".preg_replace("/'/","''",$this->sexe)."', '".preg_replace("/'/","''",$this->contact)."', '".$this->classe->id."', '".$this->date."', ".$this->amount.", '".$this->academicYear."')";
    }

    function updatePayment(){
        return "UPDATE payment SET name='".preg_replace("/'/","''",$this->name)."', matricule='".preg_replace("/'/","''",$this->matricule)."', sexe='".preg_replace("/'/","''",$this->sexe)."', contact='".preg_replace("/'/","''",$this->contact)."', idclasse='".$this->classe->id."', date='".$this->date."', amount='".$this->amount."', academicYear='".$this->academicYear."' WHERE id='".$this->id."'";
    }

    function updateProfilInfo($name, $sexe, $contact, $idclasse, $matricule){
        $name = preg_replace("/'/","''",$name);
        $contact = preg_replace("/'/","''",$contact);
        $sexe = preg_replace("/'/","''",$sexe);
        $matricule = preg_replace("/'/","''",$matricule);
        return "UPDATE payment SET name='".$name."', sexe='".$sexe."', contact='".$contact."', idclasse='".$idclasse."' WHERE matricule='".$matricule."'";
    }

    public function listRecentPayment($date){
        return "SELECT * FROM payment ".(isset($date)?("WHERE date>'".$date."'"):"")
        ." ORDER BY date DESC";
    }

    public function sumRecentPayment($date){
        return "SELECT sum(amount) as total FROM payment ".(isset($date)?("WHERE date>'".$date."'"):"");
    }

    public function countRecentPayment($date){
        return "SELECT count(id) as nbr FROM payment ".(isset($date)?("WHERE date>'".$date."'"):"");
    }

    public function listClassePayment($section,$cycle,$level,$serie,$numero, $academicYear){
        return "SELECT payment.id as id,name, matricule, sexe, contact, idclasse, date, amount, academicYear FROM classe,payment WHERE classe.id=payment.idclasse AND academicYear='".$academicYear."' ".(isset($section)?(" AND section='".$section."'"):"")
        ." ".(isset($cycle)?(" AND cycle='".$cycle."'"):"")." ".(isset($level)?(" AND level='".$level."'"):"")
        ." ".(isset($serie)?(" AND serie='".$serie."'"):"")." ".(isset($numero)?(" AND numero='".$numero."'"):"")
        ." ORDER BY name";
    }

    public function sumClassePayment($section,$cycle,$level,$serie,$numero, $academicYear){
        return "SELECT sum(payment.amount) as total FROM classe,payment WHERE classe.id=payment.idclasse AND academicYear='".$academicYear."' ".(isset($section)?(" AND section='".$section."'"):"")
        ." ".(isset($cycle)?(" AND cycle='".$cycle."'"):"")." ".(isset($level)?(" AND level='".$level."'"):"")
        ." ".(isset($serie)?(" AND serie='".$serie."'"):"")." ".(isset($numero)?(" AND numero='".$numero."'"):"");
    }

    public function countClassePayment($section,$cycle,$level,$serie,$numero, $academicYear){
        return "SELECT count(payment.id) as nbr FROM classe,payment WHERE classe.id=payment.idclasse AND academicYear='".$academicYear."' ".(isset($section)?(" AND section='".$section."'"):"")
        ." ".(isset($cycle)?(" AND cycle='".$cycle."'"):"")." ".(isset($level)?(" AND level='".$level."'"):"")
        ." ".(isset($serie)?(" AND serie='".$serie."'"):"")." ".(isset($numero)?(" AND numero='".$numero."'"):"");
    }

    public function findProfil($pattern, $academicYear){
        $motif = $pattern;
        $motif = preg_replace("/\s+/", " ", $motif);
        $parts = explode(" ", $motif);
        $motifName = "";
        for($i=0; $i<count($parts);$i++){
            $part = $parts[$i];
            $part = preg_replace("/'/", "''", $part);
            $part = strtolower($part);
            $motifName = $motifName." AND lower(name) like '%".$part."%'";
        }
        $motif = "";
        if($motifName!=''){
            $motif = $motifName;
        }
        return "SELECT DISTINCT name, matricule, sexe, contact, idclasse, academicYear, sum(amount) as amount FROM payment WHERE academicYear='".$academicYear."' ".$motif." GROUP BY name, matricule, sexe, contact, idclasse, academicYear";
    }

    public function countFindProfil($pattern, $academicYear){
        $motif = $pattern;
        $motif = preg_replace("/\s+/", " ", $motif);
        $parts = explode(" ", $motif);
        $motifName = "";
        for($i=0; $i<count($parts);$i++){
            $part = $parts[$i];
            $part = preg_replace("/'/", "''", $part);
            $part = strtolower($part);
            $motifName = $motifName." AND lower(name) like '%".$part."%'";
        }
        $motif = "";
        if($motifName!=''){
            $motif = $motifName;
        }
        return "SELECT DISTINCT count(matricule) as nbr FROM payment WHERE academicYear='".$academicYear."' ".$motif."";
    }

    public function listProfilPayment($matricule, $academicYear){
        return "SELECT id,name, matricule, sexe, contact, idclasse, date, amount, academicYear FROM payment WHERE academicYear='".$academicYear."' AND matricule='".$matricule."'";
    }

    public function sumProfilPayment($matricule, $academicYear){
        return "SELECT sum(amount) as total FROM payment WHERE academicYear='".$academicYear."' AND matricule='".$matricule."'";
    }

    public function countProfilPayment($matricule, $academicYear){
        return "SELECT count(id) as nbr FROM payment WHERE academicYear='".$academicYear."' AND matricule='".$matricule."'";
    }

    public function listPeriodPayment($dateStart, $dateEnd, $academicYear){
        if(isset($dateStart)&&isset($dateEnd)){
            return "SELECT * FROM payment WHERE date BETWEEN '".$dateStart."' AND '".$dateEnd."' AND academicYear='".$academicYear."' ORDER BY date DESC";
        }else if(isset($dateStart)){
            return "SELECT * FROM payment WHERE date>='".$dateStart."' AND academicYear='".$academicYear."' ORDER BY date DESC";
        }else if(isset($dateEnd)){
            return "SELECT * FROM payment WHERE date<='".$dateEnd."' AND academicYear='".$academicYear."' ORDER BY date DESC";
        }
        return "SELECT * FROM payment WHERE academicYear='".$academicYear."' ORDER BY date DESC";
    }

    public function sumPeriodPayment($dateStart, $dateEnd, $academicYear){
        if(isset($dateStart)&&isset($dateEnd)){
            return "SELECT sum(amount) as total FROM payment WHERE date BETWEEN '".$dateStart."' AND '".$dateEnd."' AND academicYear='".$academicYear."' ORDER BY date DESC";
        }else if(isset($dateStart)){
            return "SELECT sum(amount) as total FROM payment WHERE date>='".$dateStart."' AND academicYear='".$academicYear."' ORDER BY date DESC";
        }else if(isset($dateEnd)){
            return "SELECT sum(amount) as total FROM payment WHERE date<='".$dateEnd."' AND academicYear='".$academicYear."' ORDER BY date DESC";
        }
        return "SELECT sum(amount) as total FROM payment WHERE academicYear='".$academicYear."' ORDER BY date DESC";
    }

    public function countPeriodPayment($dateStart, $dateEnd, $academicYear){
        if(isset($dateStart)&&isset($dateEnd)){
            return "SELECT count(id) as nbr FROM payment WHERE date BETWEEN '".$dateStart."' AND '".$dateEnd."' AND academicYear='".$academicYear."' ORDER BY date DESC";
        }else if(isset($dateStart)){
            return "SELECT count(id) as nbr FROM payment WHERE date>='".$dateStart."' AND academicYear='".$academicYear."' ORDER BY date DESC";
        }else if(isset($dateEnd)){
            return "SELECT count(id) as nbr FROM payment WHERE date<='".$dateEnd."' AND academicYear='".$academicYear."' ORDER BY date DESC";
        }
        return "SELECT count(id) as nbr FROM payment WHERE academicYear='".$academicYear."' ORDER BY date DESC";
    }
}