<?php

class Classe{
    public $id;
    public $section;
    public $cycle;
    public $level;
    public $serie;
    public $numero;

    function toJson(){
        return array(
            'id' => $this->id,
            'section' => $this->section,
            'cycle' => $this->cycle,
            'level' => $this->level,
            'serie' => $this->serie,
            'numero' => $this->numero
        );
    }

    function setFromJson($json){
        $this->id = $json['id'];
        $this->section = $json['section'];
        $this->cycle = $json['cycle'];
        $this->level = $json['level'];
        $this->serie = $json['serie'];
        $this->numero = $json['numero'];
        return $this;
    }

    function setFromResultSet($row){
        $this->id = $row['id'];
        $this->section = $row['section'];
        $this->cycle = $row['cycle'];
        $this->level = $row['level'];
        $this->serie = $row['serie'];
        $this->numero = $row['numero'];
        return $this;
    }

    function removeClasse($id){
        return "DELETE FROM classe WHERE id='".$id."'";
    }

    function removeAllClasse(){
        return "DELETE FROM classe";
    }

    public function addClasse(){
        return "INSERT INTO classe (id, section, cycle, level, serie, numero) VALUES ('".$this->id."','".$this->section."', '".$this->cycle."', '".$this->level."', '".$this->serie."', '".$this->numero."')";
    }

    public function updateClasse(){
        return "UPDATE SET classe section='".$this->section."', cycle='".$this->cycle."', level='".$this->level."', serie='".$this->serie."', numero='".$this->numero."' WHERE id='".$this->id."'";
    }

    public function getClasseById($id){
        return "SELECT * FROM classe WHERE id=".$id;
    }

    public function listDataClasses($critera, $academicYear){
        return "SELECT DISTINCT classe.".$critera." FROM classe, payment WHERE academicYear='".$academicYear."'";
    }

    function listClasse($section,$cycle,$level,$serie,$numero){
        return "SELECT * FROM classe WHERE 1=1 ".(isset($section)?(" AND section='".$section."'"):"")
        ." ".(isset($cycle)?(" AND cycle='".$cycle."'"):"")." ".(isset($level)?(" AND level='".$level."'"):"")
        ." ".(isset($serie)?(" AND serie='".$serie."'"):"")." ".(isset($numero)?(" AND numero='".$numero."'"):"");
    }
}