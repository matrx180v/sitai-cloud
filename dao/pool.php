<?php

require_once 'cache.data.php';
include __DIR__.'/entities/Payment.php';

class Pool{

    public static function getDataInCacheMemory($index){ 
        $cacheMemory = new Cache();
        $result = $cacheMemory->retrieve($index);
        if($result==null){
            return null;
        }
        return $result;
    } 

    public static function setDataInCacheMemory($index, $data){
        $cacheMemory = new Cache();
        $cacheMemory->store($index, $data);
    }

    public static function resetDataInCacheMemory(){
        $cacheMemory = new Cache();
        $cacheMemory->eraseAll();
    }

    function resetCacheData(){
        Pool::resetDataInCacheMemory();
        return array(
            "response" => "ok"
        );
    }

    function getContentCacheData(){
        $cacheMemory = new Cache();
        return array(
            "response" => $cacheMemory->retrieveAll()
        );
    }

    function startTransaction($conn){
        $status = $conn->query("BEGIN;");
        return $status;
    }

    function endTransaction($conn){
        $status = $conn->query("COMMIT;");
        return $status;
    }

    function cancelTransaction($conn){
        $status = $conn->query("ROLLBACK;");
        return $status;
    }

    function setPayment($payment){
        if(!$this->addPayment($payment)){
            return $this->updatePayment($payment);
        }
        return true;
    }

    function removeProfil($matricule){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->removeProfil($matricule);
        $status = $conn->query($query);
        if(!$status) {
            return false;
        }
        Pool::resetDataInCacheMemory();
        return true;
    }

    function updateProfil($name, $sexe, $contact, $idclasse, $matricule){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->updateProfilInfo($name, $sexe, $contact, $idclasse, $matricule);
        $status = $conn->query($query);
        if(!$status) {
            return false;
        }
        Pool::resetDataInCacheMemory();
        return true;
    }

    function removePayment($id){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->removePayment($id);
        $status = $conn->query($query);
        if(!$status) {
            return false;
        }
        Pool::resetDataInCacheMemory();
        return true;
    }

    function updatePayment(Payment $payment){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $query = $payment->updatePayment();
        $status = $conn->query($query);
        if(!$status) {
            return false;
        }
        Pool::resetDataInCacheMemory();
        return true;
    }

    function addPayment(Payment $payment){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }

        $_status = $this->addClasse($payment->classe);
        if(!$_status){
            $this->updateClasse($payment->classe);
        }

        $query = $payment->addPayment();
        $status = $conn->query($query);
        
        if(!$status) {
            return false;
        }
        
        Pool::resetDataInCacheMemory();
        return true;
    }
    
    function removeAllClasse(){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $classe = new Classe();
        $query = $classe->removeAllClasse();
        $status = $conn->query($query);
        if(!$status) {
            return false;
        }
        Pool::resetDataInCacheMemory();
        return true;
    }

    function removeClasse($id){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $classe = new Classe();
        $query = $classe->removeClasse($id);
        $status = $conn->query($query);
        if(!$status) {
            return false;
        }
        Pool::resetDataInCacheMemory();
        return true;
    }

    function addClasse($classe){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $query = $classe->addClasse();
        $status = $conn->query($query);
        if(!$status) {
            return false;
        }
        Pool::resetDataInCacheMemory();
        return true;
    }

    function updateClasse($classe){
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $query = $classe->updateClasse();
        $status = $conn->query($query);
        if(!$status) {
            return false;
        }
        Pool::resetDataInCacheMemory();
        return true;
    }

    public function getClasseById($id){
        $result = Pool::getDataInCacheMemory('getClasseById-'.$id);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $classe = new Classe();
        $query = $classe->getClasseById($id);
        $sth = mysqli_query($conn, $query);
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $classe = new Classe();
                $classe->setFromResultSet($row);
            }
        }
        Pool::setDataInCacheMemory('getClasseById-'.$id, $classe);
        return $classe;
    }

    public function listRecentPaymentByPage($date, $page = '0'){
        $result = Pool::getDataInCacheMemory('listRecentPaymentByPage-'.$date."-".$page);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $rows = $this->listRecentPayment($date);
        $_index_start = intval($page)*ENV::$limit_elt_view;
        $_index_start = $_index_start>=count($rows)?count($rows):$_index_start;

        $rows = array_slice($rows, $_index_start, ENV::$limit_elt_view);
        Pool::setDataInCacheMemory('listRecentPaymentByPage-'.$date."-".$page, $rows);
        return $rows;
    }
    
    public function listRecentPayment($date){
        $result = Pool::getDataInCacheMemory('listRecentPayment-'.$date);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->listRecentPayment($date);
        $sth = mysqli_query($conn, $query);
        $rows = array();
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $_payment = new Payment();
                $_payment->setFromResultSet($row);
                $_payment->classe = $this->getClasseById($_payment->classe->id); 
                array_push($rows, $_payment->toJson());
            }
        }
        Pool::setDataInCacheMemory('listRecentPayment-'.$date, $rows);
        return $rows;
    }

    public function sumRecentPayment($date){
        $result = Pool::getDataInCacheMemory('sumRecentPayment-'.$date);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->sumRecentPayment($date);
        $sth = mysqli_query($conn, $query);
        $total = 0;
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $total = $row['total'];
            }
        }
        Pool::setDataInCacheMemory('sumRecentPayment-'.$date, $total);
        return $total;
    }

    public function countRecentPayment($date){
        $result = Pool::getDataInCacheMemory('countRecentPayment-'.$date);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->countRecentPayment($date);
        $sth = mysqli_query($conn, $query);
        $total = 0;
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $total = $row['nbr'];
            }
        }
        Pool::setDataInCacheMemory('countRecentPayment-'.$date, $total);
        return $total;
    }

    public function listClassePaymentByPage($section, $cycle, $level, $serie, $numero, $academicYear, $page){
        $result = Pool::getDataInCacheMemory('listClassePaymentByPage-'.$section."-".$cycle."-".$level."-".$serie."-".$numero."-".$academicYear."-".$page);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $rows = $this->listClassePayment($section, $cycle, $level, $serie, $numero, $academicYear);
        $_index_start = intval($page)*ENV::$limit_elt_view;
        $_index_start = $_index_start>=count($rows)?count($rows):$_index_start;

        $rows = array_slice($rows, $_index_start, ENV::$limit_elt_view);
        Pool::setDataInCacheMemory('listClassePaymentByPage-'.$section."-".$cycle."-".$level."-".$serie."-".$numero."-".$academicYear."-".$page, $rows);
        return $rows;
    }
    
    public function listClassePayment($section, $cycle, $level, $serie, $numero, $academicYear){
        $result = Pool::getDataInCacheMemory('listClassePayment-'.$section."-".$cycle."-".$level."-".$serie."-".$numero."-".$academicYear);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $rows = array();
        $payment = new Payment();
        $query = $payment->listClassePayment($section,$cycle,$level,$serie,$numero, $academicYear);
        $sth = mysqli_query($conn, $query);
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $_payment = new Payment();
                $_payment->setFromResultSet($row);
                $_payment->classe = $this->getClasseById($_payment->classe->id); 
                array_push($rows, $_payment->toJson());
            }
        }
        Pool::setDataInCacheMemory('listClassePayment-'.$section."-".$cycle."-".$level."-".$serie."-".$numero."-".$academicYear, $rows);
        return $rows;
    }

    public function sumClassePayment($section, $cycle, $level, $serie, $numero, $academicYear){
        $result = Pool::getDataInCacheMemory('sumClassePayment-'.$section."-".$cycle."-".$level."-".$serie."-".$numero."-".$academicYear);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->sumClassePayment($section,$cycle,$level,$serie,$numero, $academicYear);
        $sth = mysqli_query($conn, $query);
        $total = 0;
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $total = $row['total'];
            }
        }
        Pool::setDataInCacheMemory('sumClassePayment-'.$section."-".$cycle."-".$level."-".$serie."-".$numero."-".$academicYear, $total);
        return $total;
    }

    public function countClassePayment($section, $cycle, $level, $serie, $numero, $academicYear){
        $result = Pool::getDataInCacheMemory('countClassePayment-'.$section."-".$cycle."-".$level."-".$serie."-".$numero."-".$academicYear);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->countClassePayment($section,$cycle,$level,$serie,$numero, $academicYear);
        $sth = mysqli_query($conn, $query);
        $total = 0;
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $total = $row['nbr'];
            }
        }
        Pool::setDataInCacheMemory('countClassePayment-'.$section."-".$cycle."-".$level."-".$serie."-".$numero."-".$academicYear, $total);
        return $total;
    }

    public function listProfilPayment($matricule, $academicYear){
        $result = Pool::getDataInCacheMemory('listProfilPayment-'.$matricule."-".$academicYear);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->listProfilPayment($matricule, $academicYear);
        $sth = mysqli_query($conn, $query);
        $rows = array();
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $_payment = new Payment();
                $_payment->setFromResultSet($row);
                $_payment->classe = $this->getClasseById($_payment->classe->id); 
                array_push($rows, $_payment->toJson());
            }
        }
        Pool::setDataInCacheMemory('listProfilPayment-'.$matricule."-".$academicYear, $rows);
        return $rows;
    }

    public function sumProfilPayment($matricule, $academicYear){
        $result = Pool::getDataInCacheMemory('sumProfilPayment-'.$matricule."-".$academicYear);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->sumProfilPayment($matricule, $academicYear);
        $sth = mysqli_query($conn, $query);
        $total = 0;
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $total = $row['total'];
            }
        }
        Pool::setDataInCacheMemory('sumProfilPayment-'.$matricule."-".$academicYear, $total);
        return $total;
    }

    public function countProfilPayment($matricule, $academicYear){
        $result = Pool::getDataInCacheMemory('countProfilPayment-'.$matricule."-".$academicYear);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->countProfilPayment($matricule, $academicYear);
        $sth = mysqli_query($conn, $query);
        $total = 0;
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $total = $row['nbr'];
            }
        }
        Pool::setDataInCacheMemory('countProfilPayment-'.$matricule."-".$academicYear, $total);
        return $total;
    }

    public function listPeriodPayment($dateStart, $dateEnd, $academicYear){
        $result = Pool::getDataInCacheMemory('listPeriodPayment-'.$dateStart."-".$dateEnd."-".$academicYear);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->listPeriodPayment($dateStart, $dateEnd, $academicYear);
        $sth = mysqli_query($conn, $query);
        $rows = array();
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $_payment = new Payment();
                $_payment->setFromResultSet($row);
                $_payment->classe = $this->getClasseById($_payment->classe->id); 
                array_push($rows, $_payment->toJson());
            }
        }
        Pool::setDataInCacheMemory('listPeriodPayment-'.$dateStart."-".$dateEnd."-".$academicYear, $rows);
        return $rows;
    }

    public function listPeriodPaymentByPage($dateStart, $dateEnd, $academicYear, $page){
        $result = Pool::getDataInCacheMemory('listPeriodPaymentByPage-'.$dateStart."-".$dateEnd."-".$academicYear."-".$page);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $rows = $this->listPeriodPayment($dateStart, $dateEnd, $academicYear);
        $_index_start = intval($page)*ENV::$limit_elt_view;
        $_index_start = $_index_start>=count($rows)?count($rows):$_index_start;

        $rows = array_slice($rows, $_index_start, ENV::$limit_elt_view);
        Pool::setDataInCacheMemory('listPeriodPaymentByPage-'.$dateStart."-".$dateEnd."-".$academicYear."-".$page, $rows);
        return $rows;
    }

    public function sumPeriodPayment($dateStart, $dateEnd, $academicYear){
        $result = Pool::getDataInCacheMemory('sumPeriodPayment-'.$dateStart."-".$dateEnd."-".$academicYear);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->sumPeriodPayment($dateStart, $dateEnd, $academicYear);
        $sth = mysqli_query($conn, $query);
        $total = 0;
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $total = $row['total'];
            }
        }
        Pool::setDataInCacheMemory('sumProfilPayment-'.$dateStart."-".$dateEnd."-".$academicYear, $total);
        return $total;
    }

    public function countPeriodPayment($dateStart, $dateEnd, $academicYear){
        $result = Pool::getDataInCacheMemory('countPeriodPayment-'.$dateStart."-".$dateEnd."-".$academicYear);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->countPeriodPayment($dateStart, $dateEnd, $academicYear);
        $sth = mysqli_query($conn, $query);
        $total = 0;
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $total = $row['nbr'];
            }
        }
        Pool::setDataInCacheMemory('countPeriodPayment-'.$dateStart."-".$dateEnd."-".$academicYear, $total);
        return $total;
    }

    public function findProfilByPage($pattern, $academicYear, $page){
        $result = Pool::getDataInCacheMemory('findProfilByPage-'.$pattern."-".$academicYear."-".$page);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $rows = $this->findProfil($pattern, $academicYear);
        $_index_start = intval($page)*ENV::$limit_elt_view;
        $_index_start = $_index_start>=count($rows)?count($rows):$_index_start;

        $rows = array_slice($rows, $_index_start, ENV::$limit_elt_view);
        Pool::setDataInCacheMemory('findProfilByPage-'.$pattern."-".$academicYear."-".$page, $rows);
        return $rows;
    }

    public function findProfil($pattern, $academicYear){
        $result = Pool::getDataInCacheMemory('findProfil-'.$pattern."-".$academicYear);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->findProfil($pattern, $academicYear);
        $sth = mysqli_query($conn, $query);
        $rows = array();
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $_payment = new Payment();
                $_payment->setFromResultSet($row);
                $_payment->classe = $this->getClasseById($_payment->classe->id); 
                array_push($rows, $_payment->toJson());
            }
        }
        Pool::setDataInCacheMemory('findProfil-'.$pattern."-".$academicYear, $rows);
        return $rows;
    }
    
    public function countFindProfil($pattern, $academicYear){
        $result = Pool::getDataInCacheMemory('countFindProfil-'.$pattern."-".$academicYear);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $payment = new Payment();
        $query = $payment->countFindProfil($pattern, $academicYear);
        $sth = mysqli_query($conn, $query);
        $total = 0;
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                $total = $row['nbr'];
            }
        }
        Pool::setDataInCacheMemory('countFindProfil-'.$pattern."-".$academicYear, $total);
        return $total;
    }

    public function listDataClasses($critera, $academicYear){
        $result = Pool::getDataInCacheMemory('listDataClasses-'.$critera."-".$academicYear);
        if($result!=null){
            return $result;
        }
        $conn = ENV::getConnection();
        if($conn->connect_errno){
            return null;
        }
        $classe = new Classe();
        $query = $classe->listDataClasses($critera, $academicYear);
        $sth = mysqli_query($conn, $query);
        $rows = array();
        if (mysqli_num_rows($sth) > 0) {
            while($row = mysqli_fetch_assoc($sth)) {
                array_push($rows, $row[$critera]);
            }
        }
        Pool::setDataInCacheMemory('listDataClasses-'.$critera."-".$academicYear, $rows);
        return $rows;
    }

}