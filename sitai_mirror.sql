-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mer. 10 août 2022 à 02:48
-- Version du serveur :  8.0.22
-- Version de PHP : 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `sitai_mirror`
--

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

CREATE TABLE `classe` (
  `id` varchar(3) COLLATE utf8mb4_general_ci NOT NULL,
  `section` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cycle` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `level` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `serie` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `numero` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `classe`
--

INSERT INTO `classe` (`id`, `section`, `cycle`, `level`, `serie`, `numero`) VALUES
('10', 'Maternelle', 'Maternelle', 'GS', '', ''),
('2', 'Primaire', 'Primaire', 'SIL', '', ''),
('3', 'Primaire', 'Primaire', 'CP', '', ''),
('4', 'Primaire', 'Primaire', 'CE1', '', ''),
('5', 'Primaire', 'Primaire', 'CE2', '', ''),
('6', 'Primaire', 'Primaire', 'CM1', '', ''),
('7', 'Primaire', 'Primaire', 'CM2', '', ''),
('8', 'Maternelle', 'Maternelle', 'PS', '', ''),
('9', 'Maternelle', 'Maternelle', 'MS', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `payment`
--

CREATE TABLE `payment` (
  `id` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sexe` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `matricule` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `idclasse` varchar(3) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` float DEFAULT '0',
  `academicYear` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `payment`
--

INSERT INTO `payment` (`id`, `name`, `sexe`, `contact`, `matricule`, `idclasse`, `date`, `amount`, `academicYear`) VALUES
('157vl5fdua', 'ATSAMA REGINE CLAUDE RENEE', '/', '', '2021A060', '5', '2022-01-03 23:00:00', 5000, '2021/2022'),
('1s243fhkin', 'ANDRE ARISTIDE ELOUNDOU OWONO', '/', '', '2021A062', '2', '2022-01-10 23:00:00', 5000, '2021/2022'),
('2a34dg7j26', 'ELA SAMIRA', '/', '', '2021A140', '2', '2021-12-31 23:00:00', 15000, '2021/2022'),
('4b2ql3lpv5', 'NKOU OWONO PIERRE CELESTIN', '/', '', '2021A103', '8', '2022-01-05 23:00:00', 5000, '2021/2022'),
('5l4lsu32n0', 'MALONGO AMOUGOU BERTHE', '/', '', '2021A064', '3', '2022-01-05 23:00:00', 5000, '2021/2022'),
('6gokqvg0fd', 'TCHOUMPA ALEXIS', '/', '', '2021A086', '7', '2022-01-10 23:00:00', 5000, '2021/2022'),
('6pvgg02hh7', 'ZOCK AMOIPONG CHRIST SABIEN', '/', '', '2021A059', '6', '2022-01-03 23:00:00', 5000, '2021/2022'),
('7iusg3lbm2', 'MAI CLAUDINE', '/', '', '2021A135', '2', '2022-01-10 23:00:00', 8000, '2021/2022'),
('7lj3mqv9u7', 'MAMA ROSALIE', '/', '', '2021A101', '2', '2022-01-17 23:00:00', 3500, '2021/2022'),
('91ir2h1qfe', 'ZANG SAMIRA', '/', '', '2021A121', '5', '2022-01-16 23:00:00', 7500, '2021/2022'),
('a6im2695pu', 'TSALA VINELA', '/', '', '2021A139', '8', '2022-01-09 23:00:00', 10000, '2021/2022'),
('bn7gdu940k', 'ANDRE ARISTIDE ELOUNDOU OWONO', '/', '', '2021A062', '2', '2022-01-10 23:00:00', 5000, '2021/2022'),
('bnadks4bfs', 'MADERE MARCEL', '/', '', '2021A026', '2', '2022-01-17 23:00:00', 11000, '2021/2022'),
('cbd880sb6f', 'ZOCK MARTINE PHANELLE', '/', '', '2021A024', '5', '2022-01-05 23:00:00', 7000, '2021/2022'),
('crc46bgg64', 'AVA EMMANUEL JUNIOR', '/', '', '2021A090', '4', '2022-01-05 23:00:00', 10000, '2021/2022'),
('dof895c6cq', 'BYA ATANGANA DAVID', '/', '', '2021A025', '4', '2022-01-17 23:00:00', 11000, '2021/2022'),
('f9ms6umitg', 'ABADA NNA MERCILIA', '/', '', '2021A111', '2', '2022-01-09 23:00:00', 5000, '2021/2022'),
('fvjqd9vt9r', 'AMOUGOU HILARY', '/', '', '2021A075', '7', '2022-01-05 23:00:00', 5000, '2021/2022'),
('g33ir9sdka', 'NDJIDA VICTOR HALIDOU', '/', '', '2021A083', '8', '2022-01-10 23:00:00', 5000, '2021/2022'),
('ga2ekuhdu5', 'OYEN ATANGANA MARIETTE', '/', '', '2021A098', '3', '2022-01-03 23:00:00', 6000, '2021/2022'),
('ggqgtvs9q', 'NKE LAURENTINE', '/', '', '2021A124', '2', '2022-01-09 23:00:00', 5000, '2021/2022'),
('i4akdgtdsc', 'MBAZOA REGINE', '/', '', '2021A089', '4', '2022-01-10 23:00:00', 7000, '2021/2022'),
('i8nci633ui', 'MBALLA BENGONO AGNES', '/', '', '2021A073', '6', '2022-01-05 23:00:00', 5000, '2021/2022'),
('ikf2q7ne02', 'SEME NNA BREL', '/', '', '2021A112', '4', '2022-01-09 23:00:00', 5000, '2021/2022'),
('jj1aata54o', 'NZIGUIRI ASTA MARIE HORTENCE', '/', '', '2021A084', '5', '2022-01-10 23:00:00', 5000, '2021/2022'),
('kaaas4hpem', 'ZANG SAMIRA', '/', '', '2021A121', '5', '2022-01-16 14:38:39', 7500, '2021/2022'),
('lctiisu7eg', 'NKON FORLYNE', '/', '', '2021A122', '2', '2022-01-16 14:39:31', 7500, '2021/2022'),
('lfj2q2j3t6', 'DAVID MEDIOLO', '/', '', '2021A134', '8', '2022-01-09 23:00:00', 5000, '2021/2022'),
('m27l7mvq19', 'KOUNA BETHADA ESTHER SERENA', '/', '', '2021A094', '5', '2022-01-10 23:00:00', 5000, '2021/2022'),
('ma3uu8enu1', 'NFONDI MBALLA AROUNA', '/', '', '2021A091', '4', '2022-01-17 23:00:00', 6000, '2021/2022'),
('mb4r4bice1', 'DOUDOU ANASTASIE', '/', '', '2021A100', '8', '2022-01-17 23:00:00', 3500, '2021/2022'),
('nh0b6ebu05', 'ATANGA BIDJO\'A FRANCK', '/', '', '2021A031', '7', '2022-01-05 23:00:00', 5000, '2021/2022'),
('ojhdh8kc5u', 'ANDELA AUGUSTINE', '/', '683780234', '2021A141', '4', '2022-01-19 23:00:00', 10000, '2021/2022'),
('p66ljqd3fg', 'MAFO JULIANA', '/', '', '2021A036', '9', '2022-01-05 23:00:00', 7000, '2021/2022'),
('pn7jnrshrl', 'DOUDOU ANASTASIE', '/', '', '2021A100', '8', '2022-01-19 23:00:00', 2000, '2021/2022'),
('q632b2tnj0', 'MAMA ROSALIE', '/', '', '2021A101', '2', '2022-01-19 23:00:00', 1500, '2021/2022'),
('qc29275pkq', 'FOUDA ATANGANA LOUISE BRENDA', '/', '', '2021A099', '5', '2022-01-03 23:00:00', 11000, '2021/2022'),
('rdd78stdfa', 'MENGUE MBAZOA FLORA', '/', '', '2021A078', '4', '2022-01-05 23:00:00', 10000, '2021/2022'),
('roofoe2el7', 'ANGONO ABANDA MARTHE LEATICIA', '/', '', '2021A055', '4', '2022-01-05 23:00:00', 10000, '2021/2022'),
('s2bdiupgk4', 'MIMBE AGNES', '/', '', '2021A106', '8', '2022-01-10 23:00:00', 5000, '2021/2022'),
('sv9gses461', 'ABINHENE AMOUGOU GLWADYS', '/', '', '2021A063', '4', '2022-01-05 23:00:00', 5000, '2021/2022'),
('tmvlhcnloo', 'NYIA TSIMBI TCHOUPA', '/', '', '2021A085', '4', '2022-01-10 23:00:00', 5000, '2021/2022'),
('uriru5pb1d', 'ATSENA AMOUGOU OCEANNE PRUDENCE', '/', '', '2021A065', '8', '2022-01-05 23:00:00', 5000, '2021/2022');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `classe`
--
ALTER TABLE `classe`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_payment_classe` (`idclasse`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `fk_payment_classe` FOREIGN KEY (`idclasse`) REFERENCES `classe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
