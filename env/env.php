<?php
class ENV{
    public static $isLocalhost = true;

    public static $hostLocalDB = "localhost";
    public static $portLocalDB = "3306";
    public static $nameLocalDB = "sitai_mirror";
    public static $userLocalDB = "root";
    public static $passLocalDB = "azerty";
    
    public static $hostInternetDB = "localhost";
    public static $portInternetDB = "3306";
    public static $nameInternetDB = "id19401069_sitai_db";
    public static $userInternetDB = "id19401069_sitai_user";
    public static $passInternetDB = "Azerty@1234567890";

    public static $limit_elt_view = 30;
    public static function getLocalConnection(){
        $conn = mysqli_connect(ENV::$hostLocalDB, ENV::$userLocalDB, ENV::$passLocalDB, ENV::$nameLocalDB);
        return $conn;
    }

    public static function getInternetConnection(){
        $conn  =  new mysqli(ENV::$hostInternetDB, ENV::$userInternetDB, ENV::$passInternetDB, ENV::$nameInternetDB, ENV::$portInternetDB);
        return $conn;
    }

    public static function getConnection(){
        if(ENV::$isLocalhost){
            return ENV::getLocalConnection();
        }else{
            return ENV::getInternetConnection();
        }
    }

    public static function endsWith( $haystack, $needle ) {
        $length = strlen( $needle );
        if( !$length ) {
            return true;
        }
        return substr( $haystack, -$length ) === $needle;
    }

    public static function encodeSHA256($src){
        $code = base64_encode(hash('sha256', $src, true));
        if(ENV::endsWith($code, "=")){
            return $code;
        }
        return $code."=";
    }

    static function getValueParameter($name){
        if(isset($_POST[$name])){
            return $_POST[$name];
        }
        if(isset($_GET[$name])){
            return $_GET[$name];
        }
        $data = json_decode(file_get_contents('php://input'), true);
        if(isset($data[$name])){
            return $data[$name];
        }
        return null;
    }

}