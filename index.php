<?php

include_once __DIR__.'/env/env.php';
include_once __DIR__.'/dao/pool.php';

$action = ENV::getValueParameter('action');
$pool = new Pool();

header('Content-Type:application/json');
if($action=='reset-cache'){
    $json =  $pool->resetCacheData(); 
    print(json_encode($json));
    return;
}else if($action=='set-payment'){
    $data = ENV::getValueParameter('payment');
    $payment = new Payment();
    $payment->setFromJson($data);
    
    $pool = new Pool();
    $status = $pool->setPayment($payment);
    if($status){
        print(json_encode(array(
            'status' => '200',
            'reason' => 'successfully',
            'indexes' => []
        )));
    }else{
        print(json_encode(array(
            'status' => '500',
            'reason' => 'failed'
        )));
    }
    return;
}else if($action=='apply-last-update'){
    $datas = ENV::getValueParameter('list');
    if(isset($datas)){
        $fails = [];
        for($index=0; $index<count($datas); $index++){
            $data = $datas[$index];
            $status = true;
            $pool = new Pool();
            if($isset($data['action'])&&$data['action']=='remove-payment'){
                $status = $pool->removePayment($data['id']);
            }else if(isset($data['action'])&&$data['action']=='remove-payment'){
                $matricule = $data['matricule'];
                $name = $data['name'];
                $sexe = $data['sexe'];
                $contact = $data['contact'];
                $idclasse = $data['idclasse'];
                $status = $pool->updateProfil($name, $sexe, $contact, $idclasse, $matricule);
            }else if($isset($data['action'])&&$data['action']=='remove-eleve'){
                $matricule = $data['matricule'];
                $status = $pool->removeProfil($matricule);
            }else if($isset($data['action'])&&$data['action']=='remove-classe'){
                $status = $pool->removeClasse($data['id']);
            }else if($isset($data['action'])&&$data['action']=='remove-all'){
                $status = $pool->removeAllClasse();
            }

            if(!$status){
                array_push($fails, $index);  
            }
            
        }
        print(json_encode(array(
            'status' => '200',
            'reason' => 'successfully',
            'indexes' => $fails
        )));
        return;
    }
    print(json_encode(array(
        'status' => '500',
        'reason' => 'missing list data'
    )));
    return;
}else if($action=='set-list-payment'){
    $datas = ENV::getValueParameter('list-payment');
    if(isset($datas)){
        $fails = [];
        for($index=0; $index<count($datas); $index++){
            $data = $datas[$index];
            $payment = new Payment();
            $payment->setFromJson($data);
            $pool = new Pool();
            $status = $pool->setPayment($payment);
            if(!$status){
                array_push($fails, $index);                
            }
        }
        print(json_encode(array(
            'status' => '200',
            'reason' => 'successfully'.(count($fails)>0?(', but with problems ('.count($fails).')'):''),
            'indexes' => $fails
        )));
        return;
    }
    print(json_encode(array(
        'status' => '500',
        'reason' => 'missing list data'
    )));
    return;
}else if($action=='remove-payment'){
    $id = ENV::getValueParameter('id');
    $pool = new Pool();
    $status = $pool->removePayment($id);
    if($status){
        print(json_encode(array(
            'status' => '200',
            'reason' => 'successfully',
            'indexes' => []
        )));
    }else{
        print(json_encode(array(
            'status' => '500',
            'reason' => 'failed'
        )));
    }
    return;
}else if($action=='remove-classe'){
    $id = ENV::getValueParameter('id');
    $pool = new Pool();
    $status = $pool->removeClasse($id);
    if($status){
        print(json_encode(array(
            'status' => '200',
            'reason' => 'successfully',
            'indexes' => []
        )));
    }else{
        print(json_encode(array(
            'status' => '500',
            'reason' => 'failed'
        )));
    }
    return;
}else if($action=='update-profil'){
    $matricule = ENV::getValueParameter('matricule');
    $name = ENV::getValueParameter('name');
    $sexe = ENV::getValueParameter('sexe');
    $contact = ENV::getValueParameter('contact');
    $idclasse = ENV::getValueParameter('idclasse');
    $status = $pool->updateProfil($name, $sexe, $contact, $idclasse, $matricule);
    if($status){
        print(json_encode(array(
            'status' => '200',
            'reason' => 'successfully',
            'indexes' => []
        )));
    }else{
        print(json_encode(array(
            'status' => '500',
            'reason' => 'failed'
        )));
    }
    return;
}else if($action=='remove-eleve'){
    $matricule = ENV::getValueParameter('matricule');
    $status = $pool->removeProfil($matricule);
    if($status){
        print(json_encode(array(
            'status' => '200',
            'reason' => 'successfully',
            'indexes' => []
        )));
    }else{
        print(json_encode(array(
            'status' => '500',
            'reason' => 'failed'
        )));
    }
    return;
}else if($action=='remove-all'){    
    $status = $pool->removeAllClasse();
    if($status){
        print(json_encode(array(
            'status' => '200',
            'reason' => 'successfully',
            'indexes' => []
        )));
    }else{
        print(json_encode(array(
            'status' => '500',
            'reason' => 'failed'
        )));
    }
    return;
}else if($action=='search-profil'){
    $motif = ENV::getValueParameter('motif');
    $academicYear = ENV::getValueParameter('year');
    $page = ENV::getValueParameter('page');
    $result = $pool->findProfilByPage($motif, $academicYear, $page);
    $nbr = $pool->countFindProfil($motif, $academicYear, $page);
    print(json_encode(array(
        'status' => '200',
        'reason' => 'successfully',
        'list' => $result,
        'nbr' => $nbr
    )));
    return;
}else if($action=='list-payment'){
    $typeList = ENV::getValueParameter('type');
    if($typeList=='recent'){
        $date = ENV::getValueParameter('date');
        $page = ENV::getValueParameter('page');
        $result = $pool->listRecentPaymentByPage($date, $page);
        $total = $pool->sumRecentPayment($date);
        $nbr = $pool->countRecentPayment($date);
        print(json_encode(array(
            'status' => '200',
            'reason' => 'successfully',
            'list' => $result,
            'total' => $total,
            'nbr' => $nbr
        )));
        return;
    }else if($typeList=='classe'){
        $page = ENV::getValueParameter('page');
        $academicYear = ENV::getValueParameter('year');
        $section = ENV::getValueParameter('section');
        $cycle = ENV::getValueParameter('cycle');
        $level = ENV::getValueParameter('level');
        $serie = ENV::getValueParameter('serie');
        $numero = ENV::getValueParameter('numero');
        $result = $pool->listClassePaymentByPage($section, $cycle, $level, $serie, $numero, $academicYear, $page);
        $total = $pool->sumClassePayment($section, $cycle, $level, $serie, $numero, $academicYear);
        $nbr = $pool->countClassePayment($section, $cycle, $level, $serie, $numero, $academicYear);
        print(json_encode(array(
            'status' => '200',
            'reason' => 'successfully',
            'list' => $result,
            'total' => $total,
            'nbr' => $nbr
        )));
        return;
    }else if($typeList=='period'){
        $page = ENV::getValueParameter('page');
        $academicYear = ENV::getValueParameter('year');
        $start = ENV::getValueParameter('start');
        $end = ENV::getValueParameter('end');
        $result = $pool->listPeriodPaymentByPage($start, $end, $academicYear, $page);
        $total = $pool->sumPeriodPayment($start, $end, $academicYear);
        $nbr = $pool->countPeriodPayment($start, $end, $academicYear);
        print(json_encode(array(
            'status' => '200',
            'reason' => 'successfully',
            'list' => $result,
            'total' => $total,
            'nbr' => $nbr
        )));
        return;
    }else if($typeList=='profil'){
        $academicYear = ENV::getValueParameter('year');
        $matricule = ENV::getValueParameter('matricule');
        $result = $pool->listProfilPayment($matricule, $academicYear);
        $total = $pool->sumProfilPayment($matricule, $academicYear);
        $nbr = $pool->countProfilPayment($matricule, $academicYear);
        print(json_encode(array(
            'status' => '200',
            'reason' => 'successfully',
            'list' => $result,
            'total' => $total,
            'nbr' => $nbr
        )));
        return;
    }
    print(json_encode(array(
        'status' => '403',
        'reason' => 'Missing type list parameter'
    )));
    return;
}else if($action=='list-classe'){
    $academicYear = ENV::getValueParameter('year');
    $critera = ENV::getValueParameter('type');
    $result = $pool->listDataClasses($critera, $academicYear);
    print(json_encode($result));
    return;
}



print(json_encode(array(
    'status' => '400',
    'reason' => 'Ressource not found!!!'
)));